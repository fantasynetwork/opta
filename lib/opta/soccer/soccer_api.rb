require 'opta/api'

module Opta
  module Soccer
    class SoccerApi < Opta::Api

      def match_statistics(match)
        response_member('matchstats', match, detailed: 'yes')
      end

      # def lineups(tournament)
      #   response_collection('match', tmcl: tournament, live: 'yes', lineups: 'yes')
      # end

      def squads(tournament, team_id = nil)
        params = {tmcl: tournament, detailed: 'yes', _pgSz: 1000}
        params[:ctst] = team_id if team_id
        response_collection('squads', params)
      end

      def path
        'soccerdata'
      end

    end
  end
end
