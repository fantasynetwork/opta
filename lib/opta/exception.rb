module Opta
  class Exception < ::Exception
    def initialize(message = nil, restclient_exception = nil, url = nil)
      if(restclient_exception)
        error_code = JSON.parse(restclient_exception.response)['errorCode'].to_i
        error_message = self.class.error_message_by_code[error_code]
        message = error_message if error_message
      end
      message =  {error: message, url: url}.to_json if url
      super(message)
    end

    def self.error_message_by_code
      {
        10000 => "An unexpected error has occurred",
        10001 => "Page not found error",
        10010 => "Outlet doesn't exist",
        10100 => "Feed Unavailable",
        10200 => "Missing request parameter",
        10201 => "Unknown request parameter",
        10202 => "Invalid request parameter value",
        10203 => "Ambiguous/Invalid request parameters",
        10204 => "Invalid request method",
        10210 => "Unsupported format: _fmt parameter",
        10313 => "User not Authorised",
        10320 => "Acceptable usage exceeded",
        10400 => "No data found",
        10401 => "Data request timeout"
      }
    end
  end
end
