require 'spec_helper'
require 'opta/football/football_api'
RestClient.proxy = get_proxy

describe Opta::Football::FootballApi, vcr: {
  cassette_name: 'opta_football_api',
  record: :new_episodes,
  match_requests_on: [:uri]
} do

  let(:api) do
    Opta::Football::FootballApi.new(get_token)
  end

  let(:season_id) do
    'cjt2lss0y77rb4lcum2snklih'
  end

  let(:match_id)  do
    'f32fu9urfpt4h95tgsg3764tl'
  end

  context 'squads' do

    let(:squads) do
      api.squads(season_id)['squad']
    end

    let(:teams_name) do
      ["Miami Dolphins", "Baltimore Ravens", "Houston Texans",
       "Tennessee Titans", "Green Bay Packers", "Minnesota Vikings",
       "San Francisco 49ers", "Indianapolis Colts", "Los Angeles Chargers",
       "Los Angeles Rams", "Buffalo Bills", "Cincinnati Bengals",
       "Kansas City Chiefs", "Oakland Raiders", "Chicago Bears",
       "Tampa Bay Buccaneers", "New Orleans Saints", "New England Patriots",
       "Pittsburgh Steelers", "Cleveland Browns", "Washington Redskins",
       "New York Giants", "Dallas Cowboys", "Detroit Lions",
       "Carolina Panthers", "Seattle Seahawks", "Arizona Cardinals",
       "New York Jets", "Jacksonville Jaguars", "Denver Broncos",
       "Philadelphia Eagles", "Atlanta Falcons"]
    end

    it 'teams count' do
      expect(squads.size).to eq(32)
    end

    it 'teams players count' do
      expect(squads[6]['person'].size).to eq(96)
    end

    it 'fetches teams' do
      expect(squads.map{|team| team['contestantName']}).to eql(teams_name)
    end

  end

  context 'match_list' do

    let(:seattle_vs_minnesota_matchInfo) do
      {
        "matchInfo"=>{
          "description"=>"Seattle Seahawks vs Minnesota Vikings",
          "sport"=>{"id"=>"9ita1e50vxttzd1xll3iyaulu", "name"=>"American Football"},
          "ruleset"=>{"id"=>"523t1qaz9d9alk9mkm03f00sa", "name"=>"Men"},
          "competition"=>{"id"=>"wy3kluvb4efae1of0d8146c1", "name"=>"NFL", "country"=>{"id"=>"7hr2f89v44y65dyu9k92vprwn", "name"=>"USA"}},
          "tournamentCalendar"=>{"id"=>"cjt2lss0y77rb4lcum2snklih", "startDate"=>"2017-08-03Z", "endDate"=>"2018-02-04Z", "name"=>"NFL 2017/2018"},
          "stage"=>{"id"=>"3k3o5h8a3ga7rt349fqywaep5", "startDate"=>"2017-08-03Z", "endDate"=>"2017-08-31Z", "name"=>"Pre Season"},
          "contestant"=>[{"id"=>"bccu3mv2h2ygvz0o6tk6s77n", "name"=>"Seattle Seahawks", "country"=>{"id"=>"7hr2f89v44y65dyu9k92vprwn", "name"=>"United States"}, "position"=>"home"}, {"id"=>"8z8zg664o0q0ob4a6ab3kvspb", "name"=>"Minnesota Vikings", "country"=>{"id"=>"7hr2f89v44y65dyu9k92vprwn", "name"=>"United States"}, "position"=>"away"}],
          "venue"=>{"id"=>"8fc0yc5q303uihylozxrapxvc", "shortName"=>"CenturyLink Field"},
          "id"=>"7w3ekcltdmlbvdyzxhydra3t",
          "date"=>"2017-08-19Z", "time"=>"02:00:00Z",
          "week"=>2,
          "lastUpdated"=>"2017-08-23T22:50:45Z"}
      }
    end

    let(:match_details) do
      {
        "matchStatus"=>"Played",
        "matchType"=>"NFL",
        "matchTimeMin"=>60,
        "matchTimeSec"=>0,
        "periodTimeMin"=>0,
        "periodTimeSec"=>0,
        "periodId"=>15
      }
    end

    let(:matches) do
      api.match_list(season_id)['match']
    end

    let(:match_scores) do
      matches[300]['liveData']['matchDetails']['scores']
    end

    it 'season matches count' do
      expect(matches.size).to eq(321)
    end

    it 'count a match number of (quarters + final) scores' do
      expect(match_scores.count).to eq(5)
    end

    it 'calc match quarters scores and final score' do
      final_home_score = match_scores[match_scores.keys.last]['home']
      expect(final_home_score).to eq(20)
      expect(match_scores.first(4).map{|q| q[1]['home']}.inject(0, :+)).to eq(final_home_score)
    end

    it 'fetch a match details' do
      expect(matches[300]['liveData']['matchDetails'].reject!{|category| category == 'scores'}).to eq(match_details)
    end

    it 'fetche a season matche_info' do
      expect(matches[300].reject{|category| category == 'liveData'}).to eql(seattle_vs_minnesota_matchInfo)
    end

  end

  context 'match_statistics' do

    let(:stats) do
      api.match_statistics(match_id)
    end

    let(:lineup) do
      stats['liveData']['lineUp']
    end

    let(:darren_mcFadden) do
      {
        "firstName"=>"Darren",
        "lastName"=>"McFadden",
        "playerId"=>"ets4mg9ozgaigyb3cm6qff2eh",
        "shirtNumber"=>20,
        "position"=>"Running Back",
        "stat"=>[
          {"type"=>"longestRush", "value"=>"2"},
          {"type"=>"yardsPerRush", "value"=>"-2"},
          {"type"=>"appearances", "value"=>"1"},
          {"type"=>"rushingAttempts", "value"=>"3"},
          {"type"=>"totalRushingYards", "value"=>"-6"}]
      }
    end

    let(:jeremy_ross) do
      {"firstName"=>"Jeremy",
       "lastName"=>"Ross",
       "playerId"=>"e6zolgzhncn2pkswq3tpcsvzd",
       "shirtNumber"=>15,
       "position"=>"Wide Receiver",
       "stat"=>[
         {"type"=>"longestReception", "value"=>"29"},
         {"type"=>"longestPuntReturn", "value"=>"0"},
         {"type"=>"yardsPerReception", "value"=>"29"},
         {"type"=>"appearances", "value"=>"1"},
         {"type"=>"totalReceptions", "value"=>"1"},
         {"type"=>"receivingTargets", "value"=>"1"},
         {"type"=>"totalReceivingYards", "value"=>"29"}
       ]}
    end

    it 'count fetches match statistics teams' do
      expect(lineup.size).to eq(2)
    end

    it 'fetches match statistics teams' do
      expect(lineup[0]['contestantName']).to eq('Dallas Cowboys')
      expect(lineup[1]['contestantName']).to eq('Arizona Cardinals')
    end

    it 'count fetches match statistics players' do
      expect(lineup[0]['player'].count).to eq(60)
      expect(lineup[1]['player'].count).to eq(61)
    end

    it 'fetches match statistics players' do
      expect(lineup[0]['player'][2]).to eq(darren_mcFadden)
      expect(lineup[1]['player'][0]).to eq(jeremy_ross)
    end

  end

end
