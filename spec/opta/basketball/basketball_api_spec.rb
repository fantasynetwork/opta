require 'spec_helper'
require 'opta/basketball/basketball_api'
RestClient.proxy = get_proxy

describe Opta::Basketball::BasketballApi, vcr: {
  cassette_name: 'opta_basketball_api',
  record: :new_episodes,
  match_requests_on: [:uri]
} do

  let(:api) do
    Opta::Basketball::BasketballApi.new(get_token)
  end

  let(:season_id) do
    '8znop8ppwnewt536p1zda3pbd'
  end

  let(:match_id) do
    '4tpl1bi14dn5hu7jw57jr171l'
  end

  context 'squads' do

    let(:squads) do
      api.squads(season_id)['squad']
    end

    let(:team) do
      {
        "type"=>"club",
        "teamType"=>"default",
        "contestantId"=>"1dyqrknarjhk6bt9id8pzub43",
        "contestantName"=>"Maccabi Haifa"
      }
    end

    it 'count fetches teams' do
      expect(squads.size).to eq(35)
    end

    it 'persons count' do
      expect(squads[0]['person'].size).to eq(6)
    end

    it 'fetch a teams' do
      expect(squads[0]['contestantName']).to eql('Maccabi Haifa')
    end

    it 'team info' do
      squads
        .find {|team_hash| team_hash['contestantId'] == team['contestantId']}
        .tap do |team_hash|
        team_hash.delete('person')
        expect(team_hash).to eql(team)
      end
    end
  end

  context 'match_list' do

    let(:matches) do
      api.match_list(season_id)['match']
    end

    let(:celtics_vs_bulls) do
      {"matchInfo"=>
         {"id"=>"4tpl1bi14dn5hu7jw57jr171l",
          "date"=>"2015-12-10Z", "time"=>"00:00:00Z",
          "lastUpdated"=>"2017-07-13T12:23:26Z",
          "description"=>"Boston Celtics vs Chicago Bulls",
          "sport"=>{"id"=>"ayp4nebmprfbvzdsisazcw74y", "name"=>"basketball"}, "ruleset"=>{"name"=>"Men"},
          "competition"=>{"id"=>"e2w9iicoifx689pzi5jqw7j4a", "name"=>"NBA", "country"=>{"id"=>"7hr2f89v44y65dyu9k92vprwn", "name"=>"USA"}},
          "tournamentCalendar"=>{"id"=>"8znop8ppwnewt536p1zda3pbd", "startDate"=>"2015-10-03Z", "endDate"=>"2016-06-20Z", "name"=>"2015/2016"},
          "stage"=>{"id"=>"2pybifgzi2evov5mele3mrtnt", "startDate"=>"2015-10-28Z", "endDate"=>"2016-04-14Z", "name"=>"Regular Season"},
          "contestant"=> [{"id"=>"2w4j5p0pwwlxmcd5vceuu0rrv", "name"=>"Boston Celtics", "country"=>{"id"=>"7hr2f89v44y65dyu9k92vprwn", "name"=>"USA"}, "position"=>"home"},
                          {"id"=>"cfqu8fhvtu5hm13vrwxeh4x05", "name"=>"Chicago Bulls", "country"=>{"id"=>"7hr2f89v44y65dyu9k92vprwn", "name"=>"USA"}, "position"=>"away"}],
          "venue"=>{"id"=>"2v3g5idigftq9972bxitonzdl", "shortName"=>"TD Garden"}
         },
       "liveData"=>
         {"matchDetails"=>
            {"matchStatus"=>"Played",
             "matchType"=>"NBA",
             "matchTimeMin"=>48,
             "matchTimeSec"=> 0,
             "periodTimeMin"=>0,
             "periodTimeSec"=>0,
             "periodId"=>15,
             "scores"=>{"q1"   =>{"home"=>24, "away"=>24},
                        "q2"   =>{"home"=>27, "away"=>30},
                        "q3"   =>{"home"=>24, "away"=>21},
                        "q4"   =>{"home"=>30, "away"=>25},
                        "total"=>{"home"=>105, "away"=>100}
             }}}
      }
    end

    let(:match_details) do
      {
        "matchStatus"=>"Played",
        "matchType"=>"NBA",
        "matchTimeMin"=>48,
        "matchTimeSec"=>0,
        "periodTimeMin"=>0,
        "periodTimeSec"=>0,
        "periodId"=>15
      }
    end

    let(:match_scores) do
      matches[0]['liveData']['matchDetails']['scores']
    end

    it 'count season matches' do
      expect(matches.size).to eq(1428)
    end

    it 'check a season match data' do
      expect(matches[0]).to eql(celtics_vs_bulls)
    end

    it 'count a match number of (quarters + final) scores' do
      expect(match_scores.count).to eq(5)
    end

    it 'calc match quarters scores and final score' do
      final_home_score = match_scores[match_scores.keys.last]['home']
      expect(final_home_score).to eq(105)
      expect(match_scores.first(4).map{|q| q[1]['home']}.inject(0, :+)).to eq(final_home_score)
    end

    it 'fetch a match details' do
      expect(matches[0]['liveData']['matchDetails'].reject!{|category| category == 'scores'}).to eq(match_details)
    end

  end

  context 'match_statistics' do

    let(:avery_bradley) do
      {
        "firstName"=>"Avery",
        "lastName"=>"Bradley",
        "playerId"=>"1bn9tky8zavq51lgcr23rv1l",
        "shirtNumber"=>0,
        "position"=>"Guard",
        "stat"=>[
          {"type"=>"gameStarted", "value"=>"1"},
          {"type"=>"3PointAttempts", "value"=>"6"},
          {"type"=>"assists", "value"=>"3"},
          {"type"=>"defensiveRebounds", "value"=>"1"},
          {"type"=>"fieldGoalsAttempts", "value"=>"15"},
          {"type"=>"fieldGoalsMade", "value"=>"5"},
          {"type"=>"minsPlayed", "value"=>"38:49"},
          {"type"=>"offensiveRebounds", "value"=>"1"},
          {"type"=>"personalFouls", "value"=>"2"},
          {"type"=>"+/-", "value"=>"-1"},
          {"type"=>"steals", "value"=>"1"},
          {"type"=>"pointsScored", "value"=>"10"},
          {"type"=>"totalRebounds", "value"=>"2"},
          {"type"=>"turnovers", "value"=>"3"}
        ]
      }
    end

    let(:first_contestant_id_and_name) do
      {
        "contestantId"=>"2w4j5p0pwwlxmcd5vceuu0rrv",
        "contestantName"=>"Boston Celtics"
      }
    end

    let(:second_contestant_id_and_name) do
      {
        "contestantId"=>"cfqu8fhvtu5hm13vrwxeh4x05",
        "contestantName"=>"Chicago Bulls"
      }
    end

    let(:boston_celtics_statistics) do
      [
        {"type"=>"pointsScored", "value"=>"105"}, {"type"=>"3PointMade", "value"=>"5"},
        {"type"=>"3PointAttempts", "value"=>"23"}, {"type"=>"assists", "value"=>"22"},
        {"type"=>"biggestLead", "value"=>"10"}, {"type"=>"blocks", "value"=>"4"},
        {"type"=>"blocksAgainst", "value"=>"5"}, {"type"=>"defensiveRebounds", "value"=>"32"},
        {"type"=>"fieldGoalsAttempts", "value"=>"92"}, {"type"=>"fieldGoalsMade", "value"=>"38"},
        {"type"=>"freeThrowsMade", "value"=>"24"}, {"type"=>"freeThrowsAttempts", "value"=>"28"},
        {"type"=>"leadsTaken", "value"=>"5"}, {"type"=>"offensiveRebounds", "value"=>"13"},
        {"type"=>"personalFouls", "value"=>"22"}, {"type"=>"pointsOffTurnovers", "value"=>"13"},
        {"type"=>"steals", "value"=>"10"}, {"type"=>"fastBreakPoints", "value"=>"11"},
        {"type"=>"pointsInThePaint", "value"=>"54"}, {"type"=>"teamRebounds", "value"=>"10"},
        {"type"=>"secondChancePoints", "value"=>"17"}, {"type"=>"timesTied", "value"=>"5"},
        {"type"=>"totalRebounds", "value"=>"45"}, {"type"=>"turnovers", "value"=>"12"}
      ]

    end

    let(:stats) do
      api.match_statistics(match_id)
    end

    let(:lineup) do
      stats['liveData']['lineUp']
    end

    it 'fetch contestant id and name' do
      expect(lineup[0].reject!{|category| category == 'player' || category == 'stat' }).to eq(first_contestant_id_and_name)
      expect(lineup[1].reject!{|category| category == 'player' || category == 'stat' }).to eq(second_contestant_id_and_name)
    end

    it 'count team statistics' do
      expect(lineup.size).to eq(2)
    end

    it 'fetch contestant statistics' do
      expect(lineup[0]['stat']).to eq(boston_celtics_statistics)
    end

    it 'fetch player statistics' do
      expect(lineup[0]['player'][0]).to eq(avery_bradley)
    end

    it 'fetches match statistics' do
      expect(lineup[0]['player'].count).to eq(13)
      expect(lineup[1]['player'].count).to eq(13)
    end

  end

end
