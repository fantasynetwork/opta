require 'spec_helper'
require 'opta/soccer/soccer_api'
RestClient.proxy = get_proxy

describe Opta::Soccer::SoccerApi, vcr: {
  cassette_name: 'opta_soccer_api',
  record: :new_episodes,
  match_requests_on: [:uri]
} do

  let(:api) do
    Opta::Soccer::SoccerApi.new(get_token)
  end

  let(:season_id) do
    '1pnncrpxn8wm3s9opk8n9ozxl'
  end

  let(:match_id) do
    '91daq89hxn108p7lza5mxqkbu'
  end

  context 'squads' do

    let(:squads) do
      api.squads(season_id)['squad']
    end

    let(:squads_filtered) do
      api.squads(season_id, '9q0arba2kbnywth8bkxlhgmdr')['squad']
    end

    let(:team) do
      {
        'competitionId' => '2kwbbcootiqqgmrzs6o5inle5',
        'competitionName' => 'Premier League',
        'contestantClubName' => 'Chelsea',
        'contestantCode' => 'CHE',
        'contestantId' => '9q0arba2kbnywth8bkxlhgmdr',
        'contestantName' => 'Chelsea FC',
        'contestantShortName' => 'Chelsea',
        'teamType' => 'club',
        'type' => 'squad',
      }
    end

    it 'teams count' do
      expect(squads.size).to eq(20)
    end

    it 'can filter by squad name' do
      squad_filtered = squads_filtered.first
      squad_filtered.delete('person')
      expect(squad_filtered).to eql(team)
    end

    it 'persons count' do
      squads
        .find {|team_hash| team_hash['contestantId'] == team['contestantId']}
        .fetch('person')
        .size
        .tap {|person_count| expect(person_count).to eql(32)}
    end

    it 'team info' do
      squads
        .find {|team_hash| team_hash['contestantId'] == team['contestantId']}
        .tap do |team_hash|
        team_hash.delete('person')
        expect(team_hash).to eql(team)
      end
    end

  end

  context 'match_list' do

    let(:matches) do
      api.match_list(season_id)['match']
    end

    let(:hull_vs_leicester) do
      { 'id'=>'90kkfk8h6ea84dun94wcnk962',
        'date'=>'2017-08-11Z',
        'time'=>'18:45:00Z',
        'week'=>'1',
        'lastUpdated'=>'2017-08-11T20:39:18Z',
        'description'=>'Arsenal vs Leicester City',
        'sport'=>{'id'=>'289u5typ3vp4ifwh5thalohmq', 'name'=>'Soccer'},
        'ruleset'=>{'id'=>'79plas4983031idr6vw83nuel', 'name'=>'Men'},
        'competition'=>{'id'=>'2kwbbcootiqqgmrzs6o5inle5', 'name'=>'Premier League', 'country'=>{'id'=>'1fk5l4hkqk12i7zske6mcqju6', 'name'=>'England'}},
        'tournamentCalendar'=>{'id'=>'1pnncrpxn8wm3s9opk8n9ozxl', 'startDate'=>'2017-08-11Z', 'endDate'=>'2018-05-13Z', 'name'=>'2017/2018'},
        'stage'=>{'id'=>'1qq2l0hqb0qnbmo8dj9yd1zvt', 'formatId'=>'e2q01r9o9jwiq1fls93d1sslx', 'startDate'=>'2017-08-11Z', 'endDate'=>'2018-05-13Z', 'name'=>'Regular Season'},
        'contestant'=>[
          {'id'=>'4dsgumo7d4zupm2ugsvm4zm4d', 'name'=>'Arsenal', 'position'=>'home', 'country'=>{'id'=>'1fk5l4hkqk12i7zske6mcqju6', 'name'=>'England'}},
          {'id'=>'avxknfz4f6ob0rv9dbnxdzde0', 'name'=>'Leicester City', 'position'=>'away', 'country'=>{'id'=>'1fk5l4hkqk12i7zske6mcqju6', 'name'=>'England'}}
        ],
        'venue'=>{'id'=>'bxpq91vq4x9r3q6eq3d0bwjuy', 'neutral'=>'no', 'longName'=>'Emirates Stadium', 'shortName'=>'Emirates Stadium'}}
    end

    let(:ten_matches) do
      ["West Ham United vs Everton", "Tottenham Hotspur vs Leicester City",
       "Swansea City vs Stoke City", "Southampton vs Manchester City",
       "Newcastle United vs Chelsea", "Manchester United vs Watford",
       "Liverpool vs Brighton & Hove Albion", "Huddersfield Town vs Arsenal",
       "Crystal Palace vs West Bromwich Albion", "Burnley vs AFC Bournemouth"]
    end

    let(:west_ham_united_matches) do
      matches.select do |match|
        match['matchInfo']['contestant'][0]['name']=="West Ham United" || match['matchInfo']['contestant'][1]['name']=="West Ham United"
      end
    end

    it 'count season matches' do
      expect(matches.size).to eq(380)
    end

    it 'fetche hull vs leicester match' do
      expect(matches.last['matchInfo']).to eql(hull_vs_leicester)
    end

    it 'matches name' do
      expect(matches.map{|match| match['matchInfo']['description'] }[0...10]).to eql(ten_matches)
    end

    it 'west ham united matches count' do
      expect(west_ham_united_matches.count).to eq(38)
    end

  end

  context 'match_statistics' do

    let(:stats) do
      api.match_statistics(match_id)
    end

    let(:lineup) do
      stats['liveData']['lineUp']
    end

    let(:jakupovic) do
      {
        'playerId'=>'e00004wkpfc8jawioejmpu1n9',
        'firstName'=>'David',
        'lastName'=>'de Gea Quintana',
        'matchName'=>'David de Gea',
        'shirtNumber'=>1,
        'position'=>'Goalkeeper',
        'positionSide'=>'Centre',
        'stat'=>[
          {'type'=>'leftsidePass', 'value'=>'4'},          {'type'=>'accuratePass', 'value'=>'19'},
          {'type'=>'totalFinalThirdPasses', 'value'=>'1'}, {'type'=>'rightsidePass', 'value'=>'4'},
          {'type'=>'attemptsConcededIbox', 'value'=>'6'},  {'type'=>'touches', 'value'=>'32'},
          {'type'=>'totalFwdZonePass', 'value'=>'14'},     {'type'=>'keeperPickUp', 'value'=>'3'},
          {'type'=>'accurateFwdZonePass', 'value'=>'8'},   {'type'=>'totalChippedPass', 'value'=>'1'},
          {'type'=>'saves', 'value'=>'1'},                 {'type'=>'totalHighClaim', 'value'=>'2'},
          {'type'=>'attemptsConcededObox', 'value'=>'3'},  {'type'=>'ballRecovery', 'value'=>'8'},
          {'type'=>'possWonDef3rd', 'value'=>'3'},         {'type'=>'accurateBackZonePass', 'value'=>'11'},
          {'type'=>'passesRight', 'value'=>'9'},           {'type'=>'successfulOpenPlayPass', 'value'=>'12'},
          {'type'=>'totalBackZonePass', 'value'=>'12'},    {'type'=>'totalLongBalls', 'value'=>'17'},
          {'type'=>'accurateKeeperThrows', 'value'=>'3'},  {'type'=>'goalKicks', 'value'=>'7'},
          {'type'=>'openPlayPass', 'value'=>'14'},         {'type'=>'totalPass', 'value'=>'26'},
          {'type'=>'totalLaunches', 'value'=>'7'},         {'type'=>'fwdPass', 'value'=>'18'},
          {'type'=>'gameStarted', 'value'=>'1'},           {'type'=>'longPassOwnToOpp', 'value'=>'15'},
          {'type'=>'accurateChippedPass', 'value'=>'1'},   {'type'=>'keeperThrows', 'value'=>'3'},
          {'type'=>'goodHighClaim', 'value'=>'2'},         {'type'=>'accurateLaunches', 'value'=>'2'},
          {'type'=>'possLostAll', 'value'=>'7'},           {'type'=>'accurateLongBalls', 'value'=>'10'},
          {'type'=>'cleanSheet', 'value'=>'1'},            {'type'=>'accurateGoalKicks', 'value'=>'6'},
          {'type'=>'possLostCtrl', 'value'=>'7'},          {'type'=>'finalThirdEntries', 'value'=>'1'},
          {'type'=>'minsPlayed', 'value'=>'90'},           {'type'=>'longPassOwnToOppSuccess', 'value'=>'9'},
          {'type'=>'savedIbox', 'value'=>'1'},             {'type'=>'formationPlace', 'value'=>'1'}]
      }
    end

    it 'count fetched player match statistics' do
      expect(lineup[0]['player'].count).to eq(18)
      expect(lineup[1]['player'].count).to eq(18)
    end

    it 'fetches match player statistics' do
      expect(lineup[0]['player'].select{|p| p['lastName']=='de Gea Quintana'}.first).to eql(jakupovic)
    end

  end

  context 'response' do
    let(:match_id) do
      'adqz6a0lpkhy8kwjd13ruwfl5'
    end

    it 'handles errors' do
      expect {
        api.match_event(match_id)['liveData']['event']
      }.to raise_error('{"error":"No data found","url":"http://api.performfeeds.com/soccerdata/matchevent/{outletAuthToken}/adqz6a0lpkhy8kwjd13ruwfl5"}')
    end
  end

  context 'match_event' do

    let(:match_event) do
      { }
    end

    let(:events) do
      api.match_event(match_id)['liveData']['event']
    end

    let(:first_event) do
      {
        "id"=>1624579902,
        "eventId"=>1,
        "typeId"=>34,
        "periodId"=>16,
        "timeMin"=>0,
        "timeSec"=>0,
        "contestantId"=>"4txjdaqveermfryvbfrr4taf7",
        "outcome"=>1,
        "x"=>0.0,
        "y"=>0.0,
        "timeStamp"=>"2017-08-13T13:59:59.970Z",
        "lastModified"=>"2017-08-13T15:10:21Z",
        "qualifier"=>[ {"id"=>2121707392, "qualifierId"=>194, "value"=>"18073"},
                       {"id"=>1930163501, "qualifierId"=>44, "value"=>"1, 2, 2, 3, 2, 2, 3, 3, 4, 3, 3, 5, 5, 5, 5, 5, 5, 5"},
                       {"id"=>1532267523, "qualifierId"=>30, "value"=>"8grd4q334w1ud9vbb6vrli6z9, 4oyobe8stmkj7v4t6dci66g2d, 55bgi895g03zhc7vdh7e7yvv9, 9k1ytj3z42ggp14vkn39hiymt, eiadg033l40maayvtee9jgkwl, bmk0efhzrno1sgr3g8yijoa8l, a4qjllaut1bo0ng3x4xu5lq6t, 6fz5d1z4587tqdldr261cp1p1, djqagc8dcwm3kvsikrrirkzpx, 6uurhe88nry4hyelll8ifzd91, dccxma9p13p00n0gi1cxk1nf9, 9w5o8b18ffu4qr4bdnfrk4ogl, e211nih87ou9t8crzjbbzhexh, 2r4ox2h5r7doo3ys9ynm2nbo5, 1z3g8r34ph9ns6y1qd7qrhnmd, dd6ao1xyzj2ll9w394f250qtx, 9kvp5ddeay1cmfwoao8eo1vbp, 6jgocc12bori2wygyu07c082x"},
                       {"id"=>1087394699, "qualifierId"=>227, "value"=>"0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0"},
                       {"id"=>1585840657, "qualifierId"=>131, "value"=>"1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 0, 0, 0, 0, 0, 0, 0"},
                       {"id"=>1365920273, "qualifierId"=>197, "value"=>"435"},
                       {"id"=>1272568660, "qualifierId"=>59, "value"=>"25, 5, 26, 14, 2, 21, 20, 16, 17, 31, 27, 3, 4, 13, 15, 19, 22, 41"},
                       {"id"=>2068446461, "qualifierId"=>130, "value"=>"8"}]
      }
    end

    it 'fetches match_event' do
      expect(events.size).to eq(1727)
    end

    it 'first match event' do
      expect(events[0]).to eq(first_event)
    end

  end

  context 'tournament_schedule' do

    let(:tournament_schedule) do
      { }
    end

    let(:europa_league_id) do
      "4jvp2pwv9xdhy5udpuvwqofzd"
    end

    let(:tournament_schedule) do
      api.tournament_schedule(europa_league_id)
    end

    let(:tournament_details) do
      {
        "id"=>"4c1nfi2j1m731hcay25fcgndq",
        "name"=>"UEFA Europa League"
      }
    end

    it 'fetches tournament details' do
      expect(tournament_schedule['competition'].reject{|item| item=='lastUpdated'}).to eq(tournament_details)
    end

    it 'fetches tournament schedule' do
      expect(tournament_schedule['matchDate'].count).to eq(32)
    end

    it 'fetches tournament schedule' do
      expect(tournament_schedule['matchDate'][10]['match'].size).to eq(28)
      expect(tournament_schedule['matchDate'][10]['numberOfGames'].to_i).to eq(28)
    end

  end

end
